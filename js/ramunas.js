google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

// Draw the chart and set the chart values
function drawChart() {
  var data = google.visualization.arrayToDataTable([
  ['Task', 'Valandų per dieną'],
  ['Darbas', 7],
  ['Valgymas', 2],
  ['TV', 2],
  ['Kursai', 3],
  ['Gym', 2],
  ['Miegas', 8]
]);

  // Optional; add a title and set the width and height of the chart
  var options = {'title':'Vidutinė mano diena', 'height':400};

  // Display the chart inside the <div> element with id="piechart"
  var chart = new google.visualization.PieChart(document.getElementById('piechart'));
  chart.draw(data, options);
}  

google.charts.setOnLoadCallback(drawChart2);

function drawChart2() {
  var data = google.visualization.arrayToDataTable([
  ['Task', 'Valandų per dieną'],
  ['Darbas', 10],
  ['Valgymas', 2],
  ['TV', 2],
  ['Kursai', 3],
  ['Gym', 2],
  ['Miegas', 5]
]);

  // Optional; add a title and set the width and height of the chart
  var options = {'title':'Vidutinė mano diena', 'height':400};

  // Display the chart inside the <div> element with id="piechart"
  var chart = new google.visualization.PieChart(document.getElementById('piechart1'));
  chart.draw(data, options);
}


    google.charts.setOnLoadCallback(drawChart3);
    function drawChart3() {
      var data = google.visualization.arrayToDataTable([
        ["Prekės", "Suma eurais / mėn.", { role: "style" } ],
        ["Rūbai", 200, "#930b36"],
        ["Maistas", 400, "#0dadaa"],
        ["Laisvalaikis", 200, "#27db1a"],
        ["Rezervas", 200, "color: #c1770f"]
      ]);

      var view = new google.visualization.DataView(data);
      view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

      var options = {
        title: "Pirkinių krepšelis",
        height: 400,
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
      };
      var chart = new google.visualization.BarChart(document.getElementById("chart3"));
      chart.draw(view, options);
  }
document.addEventListener('DOMContentLoaded', function() {
elems = document.querySelectorAll('.carousel');
    
    var instance = M.Carousel.init(elems,{
	    fullWidth: true,
	    indicators: true
	  });
  });
