  document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.dropdown-trigger');
    var instances = M.Dropdown.init(elems);
  });

    document.addEventListener('DOMContentLoaded', function() {
    elems = document.querySelectorAll('.carousel');
    
    var instance = M.Carousel.init(elems,{
	    fullWidth: true,
	    indicators: true
	  });
  });

google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

// Draw the chart and set the chart values
function drawChart() {
  var data = google.visualization.arrayToDataTable([
  ['VEIKLA', 'Valandos per dieną'],
  ['Darbas', 8],
  ['Valgymas', 1],
  ['Laisvas laikas (knygos, tv, filmai)', 2],
  ['Kursai', 3],
  ['Šuns vedžiojimas ir auklėjimas', 2],
  ['Miegas', 6],
  ['Važinėjimas', 1]
]);

  // Optional; add a title and set the width and height of the chart
  var options = {'title':'Vidutinė mano diena', 'height':400};

  // Display the chart inside the <div> element with id="piechart"
  var chart = new google.visualization.PieChart(document.getElementById('piechart'));
  chart.draw(data, options);
}  

var ctx = document.getElementById('doughnut1').getContext('2d');
var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'doughnut',

    // The data for our dataset
    data: {
        labels: ["January", "February", "March", "April", "May", "June", "July"],
        datasets: [{
            hoverBackgroundColor: 'rgb(255, 99, 132)',
            borderColor: 'rgb(241, 148, 138)',
            data: [15, 10, 5, 2, 20, 30, 45],
        }]
    },
   options: {
   	title: {
   		display: true,
   		text:'Spurgų kiekis per mėnesį'
   	} 
   }
});


var ctx = document.getElementById('linija').getContext('2d');
var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'line',

    // The data for our dataset
    data: {
        labels: ['1 savaite', '2 savaite', '3 savaite', '4 savaite', '5 savaite', '6 savaite'],
 
        datasets:[{
        	label: ['Mindaugo'],
        	data: [26, 30, 45, 15, 23, 33],
        	borderColor: 'rgb(93, 109, 126)',
        }, {
        	label: ['Dariušo'],
        	data: [40, 22, 15, 34, 21, 37],
        	borderColor: 'rgb(255, 0, 64)',
        }]
    },
   options: {
   	title: {
   		display: true,
   		text:'Kodinimas per savaitę',
   	} 
   }
});

