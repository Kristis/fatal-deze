google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

// Draw the chart and set the chart values

 function drawChart() {
  var data = google.visualization.arrayToDataTable([
  ['Task', 'diena'],
  ['Pirmadienis', 1],
  ['Antradienis', 2],
  ['Treciadienis', 3],
  ['Ketvirtadienis', 4],
  ['Penktadienis', 5],
  ['Sestadienis', 6],
  ['Sekmadienis', 7]
]);

  // Optional; add a title and set the width and height of the chart
  var options = {'title':'Megstamiausia diena', 'height':400};

  // Display the chart inside the <div> element with id="piechart"
  var chart = new google.visualization.PieChart(document.getElementById('piechart'));
  chart.draw(data, options);
}  

google.charts.setOnLoadCallback(drawChart2);

function drawChart2() {
  var data = google.visualization.arrayToDataTable([
  ['Task', 'Valandų per dieną'],
  ['Darbas', 5],
  ['Valgymas', 5],
  ['TV', 4],
  ['Kursai', 6],
  ['Gym', 2],
  ['Miegas', 2],
  ['Laisvalaikis', 3]
]);

  // Optional; add a title and set the width and height of the chart
  var options = {'title':'Vidutinė mano diena', 'height':400};

  // Display the chart inside the <div> element with id="piechart"
  var chart = new google.visualization.PieChart(document.getElementById('piechart1'));
  chart.draw(data, options);
}


    google.charts.setOnLoadCallback(drawChart3);
    function drawChart3() {
      var data = google.visualization.arrayToDataTable([
        ["Menuo", "Suma eurais / mėn.", { role: "style" } ],
        ["Lapkritis", 10, "#930b36"],
        ["Gruodis", 25, "#0dadaa"],
        ["Sausis", 35, "#27db1a"],
        ["Vasaris", 20, "color: #c1770f"],
        ["kovas", 15, "color: #FF00FF"]
      ]);

      var view = new google.visualization.DataView(data);
      view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

      var options = {
        title: "Sildymo saskaita",
        height: 400,
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
      };
      var chart = new google.visualization.BarChart(document.getElementById("chart3"));
      chart.draw(view, options);
  }
document.addEventListener('DOMContentLoaded', function() {
elems = document.querySelectorAll('.carousel');
    
    var instance = M.Carousel.init(elems,{
	    fullWidth: true,
	    indicators: true
	  });
  });
